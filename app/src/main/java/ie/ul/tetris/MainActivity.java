package ie.ul.tetris;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ie.ul.tetris.game.TetrisActivity;
import ie.ul.tetris.leaderboards.LeaderboardsActivity;

public class MainActivity extends Activity
{
    private static final String LOG_TAG =MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //creates Option button
        Button OptionsButton = findViewById(R.id.button);
        OptionsButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View v)
            {
                launchOptionsActivity();
            }
        });
        //Creates Play button
        Button PlayButton= findViewById(R.id.button2);
        PlayButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                launchTetrisActivity();
            }
        });

        final Button leaderboardsButton = findViewById(R.id.button3);
        leaderboardsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                launchLeaderboardsActivity();
            }
        });

    }

    private void launchTetrisActivity()
    {
        Intent PlayIntent = new Intent( this, TetrisActivity.class);
        startActivity(PlayIntent);
    }
    private void launchOptionsActivity()
    {
        Intent OptionsIntent = new Intent(this, OptionsActivity.class);
        startActivity(OptionsIntent);
    }

    private void launchLeaderboardsActivity()
    {
        final Intent launchIntent = new Intent(this, LeaderboardsActivity.class);
        startActivity(launchIntent);
    }
}