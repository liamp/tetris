package ie.ul.tetris;

public final class Extra {
    public static final String SCORE_VALUE = "Score_Value";
    public static final String LEADERBOARD_PAGE = "Leaderboard_Page";
    public static final String GAME_SPEED = "GameSpeed";

    // No-op constructor
    private Extra() {}
}
