package ie.ul.tetris;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by Eirc Nolan on 15-Apr-18.
 */

public class SpeedActivity extends Activity
{

    private static final String LOG_TAG = SpeedActivity.class.getSimpleName();

    private static final int SPEED_SLOW = 150;
    private static final int SPEED_NORMAL = 100;
    private static final int SPEED_FAST = 50;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.speedmenu);


        Button SlowButton = findViewById(R.id.Slowbutton);
        SlowButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SlowGame();
            }
        });

        Button NormalButton = findViewById(R.id.NormButton);
        NormalButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                NormalGame();
            }
        });

        Button FastButton = findViewById(R.id.FastButton);
        FastButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                FastGame();
            }
        });
    }

    private void SlowGame()
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(Extra.GAME_SPEED, SPEED_SLOW);
        editor.apply();

        Toast.makeText(this.getApplicationContext(), "Speed set to SLOW", Toast.LENGTH_SHORT).show();
    }

    private void NormalGame()
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(Extra.GAME_SPEED, SPEED_NORMAL);
        editor.apply();

        Toast.makeText(this.getApplicationContext(), "Speed set to NORMAL", Toast.LENGTH_SHORT).show();
    }

    private void FastGame()
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor =  preferences.edit();
        editor.putInt(Extra.GAME_SPEED, SPEED_FAST);
        editor.apply();

        Toast.makeText(this.getApplicationContext(), "Speed set to FAST", Toast.LENGTH_SHORT).show();
    }
}
