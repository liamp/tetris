package ie.ul.tetris.leaderboards;


import android.support.annotation.Keep;
import android.support.annotation.NonNull;

import java.io.Serializable;

/**
 * Models a high-score at a game of Tetris.
 *
 * @author Liam Power
 */
@Keep
final class TetrisScore implements Serializable {

    @Keep
    public int score;

    @Keep
    public String name;

    TetrisScore(@NonNull final String name, final int score) {
        this.score = score;
        this.name = name;
    }

    // No-op private constructor; this exists for serialization from Firebase. Clients in code
    // should *NEVER* call this constructor as it results in an uninitialized object.
    @Keep
    private TetrisScore() { }

    int getScore() {
        return this.score;
    }

    @NonNull
    String getName() {
        return this.name;
    }
}
