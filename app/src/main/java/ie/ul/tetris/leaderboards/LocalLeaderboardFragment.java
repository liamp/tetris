package ie.ul.tetris.leaderboards;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;

import ie.ul.tetris.Extra;
import ie.ul.tetris.R;

public class LocalLeaderboardFragment extends Fragment {
    private static final String LOCAL_LEADERBOARDS_FILENAME = "local_leaderboard";
    private static final String LOG_TAG = LocalLeaderboardFragment.class.getSimpleName();

    private TetrisScoreAdapter adapter;

    private ArrayList<TetrisScore> getScores() throws IOException {
        final ArrayList<TetrisScore> scores = new ArrayList<>();
        final File leaderboardsFile = new File(this.getContext().getFilesDir(), LOCAL_LEADERBOARDS_FILENAME);

        if (leaderboardsFile.exists()) {
            final InputStream input = this.getContext().openFileInput(LOCAL_LEADERBOARDS_FILENAME);
            final BufferedReader reader = new BufferedReader(new InputStreamReader(input, "UTF-8"));

            String line;
            while ((line = reader.readLine()) != null) {
                final String[] data = line.split(":");
                scores.add(new TetrisScore(data[0], Integer.parseInt(data[1])));
            }
        }

        return scores;
    }

    private void deleteScore(int deletePos) {
        for (int scoreIndex = 0; scoreIndex < this.adapter.getCount(); ++scoreIndex) {
            if (deletePos == scoreIndex) {
                this.adapter.remove(this.adapter.getItem(scoreIndex));
                this.adapter.notifyDataSetChanged();
                try {
                    this.saveScoreData();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                    Toast.makeText(this.getContext(),
                            "Error occurred while writing local score data file " + ioException.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    private void setScore(TetrisScore score) {
        this.adapter.add(score);
        this.adapter.notifyDataSetChanged();
        try {
            this.saveScoreData();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    private void saveScoreData() throws IOException {
        final File leaderboardsFile = new File(this.getContext().getFilesDir(), LOCAL_LEADERBOARDS_FILENAME);
        if (leaderboardsFile.createNewFile()) {
            Log.i(LOG_TAG, "Created local leaderboards file");
        }

        final OutputStream out = this.getContext().openFileOutput(LOCAL_LEADERBOARDS_FILENAME, Context.MODE_PRIVATE);
        for (int scoreIndex = 0; scoreIndex < this.adapter.getCount(); ++scoreIndex) {
            final TetrisScore score = this.adapter.getItem(scoreIndex);
            final String scoreLine = score.getName() + ":" + score.getScore() + "\n";

            out.write(scoreLine.getBytes());
        }
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup)inflater.inflate(R.layout.activity_leaderboards, container, false);
        rootView.findViewById(R.id.leaderboardsLoading).setVisibility(View.GONE);

        final ArrayList<TetrisScore> scores;

        try {
            scores = this.getScores();
            this.adapter = new TetrisScoreAdapter(this.getContext(), scores);

            if (this.getArguments().containsKey("Score_Username")) {
                if (this.getArguments().containsKey(Extra.LEADERBOARD_PAGE) &&
                    this.getArguments().getInt(Extra.LEADERBOARD_PAGE) == LeaderboardsActivity.PAGE_LOCAL) {
                    final String username = this.getArguments().getString("Score_Username");
                    final int score = this.getArguments().getInt("Score_Value", -1);
                    this.setScore(new TetrisScore(username, score));

                    Toast.makeText(this.getContext(), R.string.leaderboards_add_success, Toast.LENGTH_SHORT).show();
                }
            }


            if (!scores.isEmpty()) {
                final ListView scoresListView = rootView.findViewById(R.id.leaderboards_score_list);
                scoresListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int pos, long id) {
                        deleteScore(pos);
                        return true;
                    }
                });
                final TextView leaderboardsTitle = rootView.findViewById(R.id.leaderboards_title);
                leaderboardsTitle.setText(R.string.leaderboards_title_local);
                scoresListView.setAdapter(this.adapter);
            }
            else {
                rootView = (ViewGroup)inflater.inflate(R.layout.leaderboards_empty, container, false);
            }


        } catch (Exception e) {
            e.printStackTrace();

            Toast.makeText(this.getContext(), R.string.leaderboards_add_failure, Toast.LENGTH_SHORT).show();
        }

        return rootView;
    }
}
