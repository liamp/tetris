package ie.ul.tetris.leaderboards;


import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import ie.ul.tetris.R;

/**
 * Used to adapt {@link ie.ul.tetris.leaderboards.TetrisScore} to an Android ListView.
 */
final class TetrisScoreAdapter extends ArrayAdapter<TetrisScore> {

    private final ArrayList<TetrisScore> scores;
    TetrisScoreAdapter(Context context, ArrayList<TetrisScore> scores) {
        super(context, 0, scores);
        this.scores = scores;
    }

    @NonNull
    @Override
    public View getView(int index, View convertView, @NonNull ViewGroup parent) {
        TetrisScore score = this.scores.get(index);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_score, parent, false);
        }

        TextView tvUsername = convertView.findViewById(R.id.leaderboards_list_item_name);
        TextView tvScore = convertView.findViewById(R.id.leaderboards_list_item_score);

        if (score != null) {
            tvUsername.setText(score.getName());
            tvScore.setText(String.valueOf(score.getScore()));
        }


        return convertView;
    }
}
