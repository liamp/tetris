package ie.ul.tetris.leaderboards;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.Comparator;

import ie.ul.tetris.Extra;
import ie.ul.tetris.R;

public class FirebaseLeaderboardFragment extends Fragment
{
    private static final String LOG_TAG = FirebaseLeaderboardFragment.class.getSimpleName();

    private final ArrayList<TetrisScore> scores = new ArrayList<>();
    private TetrisScoreAdapter scoreListAdapter;
    private DatabaseReference db;

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.activity_leaderboards, container, false);

        this.db = FirebaseDatabase.getInstance().getReference();
        final Query query = db.child("scores");

        final ListView scoresListView = rootView.findViewById(R.id.leaderboards_score_list);
        final TextView leaderboardsTitle = rootView.findViewById(R.id.leaderboards_title);
        leaderboardsTitle.setText(R.string.leaderboards_title_online);
        this.scoreListAdapter = new TetrisScoreAdapter(this.getContext(), this.scores);
        scoresListView.setAdapter(scoreListAdapter);

        final ProgressBar loadingIndicator = rootView.findViewById(R.id.leaderboardsLoading);
        final Bundle args = this.getArguments();
        if (args != null && args.containsKey("Score_Username")) {
            if (args.containsKey(Extra.LEADERBOARD_PAGE) &&
                    args.getInt(Extra.LEADERBOARD_PAGE) == LeaderboardsActivity.PAGE_ONLINE) {
                String username = args.getString("Score_Username");
                if (username == null) {
                    username = "no_user";
                }
                final int score = args.getInt("Score_Value", -1);
                this.setScore(new TetrisScore(username, score));

                Toast.makeText(this.getContext(), R.string.leaderboards_add_success, Toast.LENGTH_SHORT).show();
            }
        }

        final Comparator<TetrisScore> scoreComparator = new Comparator<TetrisScore>() {
            @Override
            public int compare(TetrisScore a, TetrisScore b) {
                return b.getScore() - a.getScore();
            }
        };

        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                final TetrisScore score = dataSnapshot.getValue(TetrisScore.class);

                if (score != null) {
                    scoreListAdapter.add(score);
                    scoreListAdapter.sort(scoreComparator);
                    scoreListAdapter.notifyDataSetChanged();
                }

                loadingIndicator.setVisibility(View.GONE);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                final TetrisScore changedScore = dataSnapshot.getValue(TetrisScore.class);

                if (changedScore != null) {
                    for (int scoreIndex = 0; scoreIndex < scoreListAdapter.getCount(); ++scoreIndex) {
                        final TetrisScore storedScore = scoreListAdapter.getItem(scoreIndex);

                        if (storedScore != null && storedScore.getName().equals(changedScore.getName())) {
                            scoreListAdapter.remove(storedScore);
                            scoreListAdapter.add(changedScore);
                            scoreListAdapter.sort(scoreComparator);
                            scoreListAdapter.notifyDataSetChanged();
                            break;
                        }
                    }
                }

                loadingIndicator.setVisibility(View.GONE);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                final TetrisScore removedScore = dataSnapshot.getValue(TetrisScore.class);
                if (removedScore != null) {
                    scoreListAdapter.remove(removedScore);
                    scoreListAdapter.sort(scoreComparator);
                    scoreListAdapter.notifyDataSetChanged();
                }

                loadingIndicator.setVisibility(View.GONE);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(LOG_TAG, "Failed to load firebase high scores!");
                Log.e(LOG_TAG, databaseError.toString());
                final View failedToLoadLeaderboardMsg = inflater.inflate(R.layout.leaderboards_msg_error, container, false);
                rootView.removeAllViews();
                rootView.addView(failedToLoadLeaderboardMsg);
                loadingIndicator.setVisibility(View.GONE);
            }
        });

        return rootView;
    }

    private void setScore(final TetrisScore score) {
        Log.i(LOG_TAG, "Begin set score");
        final FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        Log.e(LOG_TAG, "Score username" + score.getName());
        if (currentUser != null) {
            this.db.child("scores").child(currentUser.getUid()).setValue(score);
        }

    }
}
