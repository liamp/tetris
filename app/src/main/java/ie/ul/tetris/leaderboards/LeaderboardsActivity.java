package ie.ul.tetris.leaderboards;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

import ie.ul.tetris.Extra;
import ie.ul.tetris.MainActivity;
import ie.ul.tetris.R;

public class LeaderboardsActivity extends FragmentActivity
{
    private static final String LOG_TAG = LeaderboardsActivity.class.getSimpleName();

    private static final int PAGE_COUNT = 2;

    public static final int PAGE_ONLINE = 0;
    public static final int PAGE_LOCAL = 1;

    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.leaderboards_slider);
        this.viewPager = findViewById(R.id.pager);
        final Bundle fragmentArgs = new Bundle();

        if (this.getIntent().hasExtra("Score_Username"))
        {
            final String username = this.getIntent().getStringExtra("Score_Username");
            final int score = this.getIntent().getIntExtra("Score_Value", -999);

            fragmentArgs.putString("Score_Username", username);
            fragmentArgs.putInt("Score_Value", score);
        }

        final PagerAdapter pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(), fragmentArgs);
        this.viewPager.setAdapter(pagerAdapter);

        if (this.getIntent().hasExtra(Extra.LEADERBOARD_PAGE))
        {
            final int page = this.getIntent().getIntExtra(Extra.LEADERBOARD_PAGE, -1);
            Log.e(LOG_TAG, "Send leaderboard page intent as " + page);
            this.viewPager.setCurrentItem(page);
            fragmentArgs.putInt(Extra.LEADERBOARD_PAGE, this.getIntent().getIntExtra(Extra.LEADERBOARD_PAGE, PAGE_ONLINE));
        }
    }

    @Override
    public void onBackPressed()
    {
        if (this.viewPager.getCurrentItem() == 0)
        {
            // Go to the main menu if we're on the first page
            final Intent goToMainMenu = new Intent(this.getApplicationContext(), MainActivity.class);
            startActivity(goToMainMenu);
        }
        else
        {
            // Go back a page otherwise
            this.viewPager.setCurrentItem(this.viewPager.getCurrentItem() - 1);
        }
    }


    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        private final Bundle args;
        ScreenSlidePagerAdapter(FragmentManager fm, Bundle args) {
            super(fm);
            this.args = args;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == PAGE_ONLINE)
            {
                final FirebaseLeaderboardFragment fragment = new FirebaseLeaderboardFragment();
                fragment.setArguments(this.args);
                return fragment;
            }
            else if (position == PAGE_LOCAL)
            {
                final LocalLeaderboardFragment fragment =  new LocalLeaderboardFragment();
                fragment.setArguments(this.args);
                return fragment;
            }

            return null; // Shouldn't happen
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }
    }
}
