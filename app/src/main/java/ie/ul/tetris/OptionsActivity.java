package ie.ul.tetris;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class OptionsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);

        Button helpButton = findViewById(R.id.MainMenuButton);
        helpButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View v)
            {
                LaunchHelpActivity();
            }

        });

        Button SpeedButton = findViewById(R.id.SpeedButton);
        SpeedButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View v)
            {
                launchSpeedActivity();
            }
        });

        final Button signOutButton = findViewById(R.id.SignOutButton);
        final FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser == null) {
            signOutButton.setEnabled(false);
            findViewById(R.id.optionsMsgNotSignedIn).setVisibility(View.VISIBLE);
        }
    }
    private void LaunchHelpActivity()
    {
        Intent goToHelpIntent = new Intent(this, HelpActivity.class);
        startActivity(goToHelpIntent);
    }

    private void launchSpeedActivity() {
        Intent SpeedIntent = new Intent(this, SpeedActivity.class);
        startActivity(SpeedIntent);
    }

    public void onSignoutButtonClicked(final View view) {
        final FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            AuthUI.getInstance().signOut(this).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    Toast.makeText(getApplicationContext(), "You have been signed out of the leaderboards system", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

}
