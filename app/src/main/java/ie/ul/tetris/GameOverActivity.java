package ie.ul.tetris;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;
import java.util.List;

import ie.ul.tetris.game.TetrisActivity;
import ie.ul.tetris.leaderboards.LeaderboardsActivity;


public class GameOverActivity extends Activity
{
    private static final int SIGNUP_REQUEST_CODE = 0xBADBABE;

    private int score = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gameover);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.score = getIntent().getIntExtra(Extra.SCORE_VALUE, -1);
            final String s = String.valueOf(this.score);
            final TextView ScoreTextView = findViewById(R.id.ScoreView);

            ScoreTextView.setText(s);
        }

        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            final Button btnUploadScore = findViewById(R.id.btnUploadScore);
            btnUploadScore.setText(R.string.btn_upload_text_sign_in);
        }

    }

    @Override
    public void onBackPressed() {
        final Intent goToMainMenuIntent = new Intent(this.getApplicationContext(), MainActivity.class);
        startActivity(goToMainMenuIntent);
    }

    public void onSaveScoreClicked(final View view) {
        AlertDialog.Builder usernameInputDialog = new AlertDialog.Builder(this);
        usernameInputDialog.setTitle("Enter your username");

        final EditText usernameInputField = new EditText(this);
        usernameInputField.setInputType(InputType.TYPE_CLASS_TEXT);
        usernameInputDialog.setView(usernameInputField);

        usernameInputDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final String username = usernameInputField.getText().toString();

                final Intent saveScoreIntent = new Intent(getApplicationContext(), LeaderboardsActivity.class);
                saveScoreIntent.putExtra(Extra.LEADERBOARD_PAGE, LeaderboardsActivity.PAGE_LOCAL);
                saveScoreIntent.putExtra("Score_Username", username);
                saveScoreIntent.putExtra("Score_Value", score);

                startActivity(saveScoreIntent);
            }
        });

        usernameInputDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        usernameInputDialog.show();
    }

    public void onUploadScoreClicked(final View view) {
        // Check if we're signed in
        final FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            final String username = currentUser.getDisplayName();
            if (username == null) {
                Log.e("GameOverActivity", "Error - username null");
            }
            final int score = this.score;

            final Intent uploadScoreIntent = new Intent(this.getApplicationContext(), LeaderboardsActivity.class);
            uploadScoreIntent.putExtra(Extra.LEADERBOARD_PAGE, LeaderboardsActivity.PAGE_ONLINE);
            uploadScoreIntent.putExtra("Score_Username", username);
            uploadScoreIntent.putExtra("Score_Value", score);

            startActivity(uploadScoreIntent);
        } else {
            // Start sign-in flow
            List<AuthUI.IdpConfig> providers = Arrays.asList(
                    new AuthUI.IdpConfig.EmailBuilder().build(),
                    new AuthUI.IdpConfig.GoogleBuilder().build());

            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(providers)
                            .build(),
                    SIGNUP_REQUEST_CODE);
        }
    }

    public void onTryAgainClicked(final View view) {
        final Intent retryGameIntent = new Intent(this.getApplicationContext(), TetrisActivity.class);
        startActivity(retryGameIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SIGNUP_REQUEST_CODE) {
            final IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                final String username = FirebaseAuth.getInstance().getCurrentUser().getDisplayName();
                if (username != null) {
                    final int score = this.score;

                    final Intent uploadScoreIntent = new Intent(this.getApplicationContext(), LeaderboardsActivity.class);
                    uploadScoreIntent.putExtra(Extra.LEADERBOARD_PAGE, LeaderboardsActivity.PAGE_ONLINE);
                    uploadScoreIntent.putExtra("Score_Username", username);
                    uploadScoreIntent.putExtra("Score_Value", score);

                    startActivity(uploadScoreIntent);
                } else {
                    Log.e("GameOverActivity", "Error - username null");
                }
            } else {

                if (response != null) {
                    Log.e("GameOverActivity", "RESPONSE:" + response.toString());
                }

                final Intent goToMainMenu = new Intent(this.getApplicationContext(), MainActivity.class);
                startActivity(goToMainMenu);

            }
        }
    }
}


