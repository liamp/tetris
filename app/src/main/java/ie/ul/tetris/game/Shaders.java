package ie.ul.tetris.game;

import android.util.Log;

import static android.opengl.GLES20.GL_COMPILE_STATUS;
import static android.opengl.GLES20.GL_FRAGMENT_SHADER;
import static android.opengl.GLES20.GL_LINK_STATUS;
import static android.opengl.GLES20.GL_VERTEX_SHADER;
import static android.opengl.GLES20.glAttachShader;
import static android.opengl.GLES20.glCompileShader;
import static android.opengl.GLES20.glCreateProgram;
import static android.opengl.GLES20.glCreateShader;
import static android.opengl.GLES20.glDeleteShader;
import static android.opengl.GLES20.glGetProgramInfoLog;
import static android.opengl.GLES20.glGetProgramiv;
import static android.opengl.GLES20.glGetShaderInfoLog;
import static android.opengl.GLES20.glGetShaderiv;
import static android.opengl.GLES20.glLinkProgram;
import static android.opengl.GLES20.glShaderSource;

final class Shaders {

    /** No constructor required for static class **/
    private Shaders() {}


    static final String MAIN_VERTEX =
            "uniform mat4 transform;" +
            "attribute vec2 vPosition;" +
            "void main() {" +
            "  gl_Position = transform * vec4(vPosition.xy, 0.0, 1.0);" +
            "}";

    static final String MAIN_FRAGMENT =
            "precision mediump float;" +
            "uniform vec3 vColor;" +
            "void main() {" +
            "  gl_FragColor = vec4(vColor.r, vColor.g, vColor.b, 1.0);" +
            "}";


    static int compile(final String vsCode, final String fsCode) {
        int vShader = glCreateShader(GL_VERTEX_SHADER);
        int fShader = glCreateShader(GL_FRAGMENT_SHADER);
        int program = glCreateProgram();
        int compileStatus[] = new int[1];
        int linkStatus[] = new int[1];

        glShaderSource(vShader, vsCode);
        glShaderSource(fShader, fsCode);

        glCompileShader(vShader);
        glGetShaderiv(vShader, GL_COMPILE_STATUS, compileStatus, 0);
        if (compileStatus[0] == 0) {
            Log.e("Tetris/Compile_Shader", "Failed to compile vertex shader");
            Log.e("Tetris/Compile_Shader", glGetShaderInfoLog(vShader));

            return 0;
        }

        glCompileShader(fShader);
        glGetShaderiv(fShader, GL_COMPILE_STATUS, compileStatus, 0);
        if (compileStatus[0] == 0) {
            Log.e("Tetris/Compile_Shader", "Failed to compile fragment shader");
            Log.e("Tetris/Compile_Shader", glGetShaderInfoLog(fShader));

            return 0;
        }

        glAttachShader(program, vShader);
        glAttachShader(program, fShader);

        glLinkProgram(program);
        glGetProgramiv(program, GL_LINK_STATUS, linkStatus, 0);
        if (linkStatus[0] == 0) {
            Log.e("Tetris/Link_Shader", "Failed to link shader program");
            Log.e("Tetris/Link_Shader", glGetProgramInfoLog(program));

            return 0;
        }

        // Safe to delete individual shader objects once they're linked into the program
        glDeleteShader(vShader);
        glDeleteShader(fShader);

        return program;
    }

}
