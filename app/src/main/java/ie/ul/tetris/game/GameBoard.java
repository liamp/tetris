package ie.ul.tetris.game;


import java.util.Arrays;

/**
 * Provides an interface to query and update the state of a Tetris game board.
 *
 * @author Daniel Keighley
 * @author Liam Power
 */
final class GameBoard {

    static final int VISIBLE_ROWS = 25;
    static final int VISIBLE_COLS = 10;

    private static final int ROWS = 29;
    private static final int COLS = 10;

    private static final byte CELL_EMPTY = 0;
    private static final byte CELL_FILLED = 1;
    private static final byte CELL_FALLING = 2;

    private static final int DEFAULT_TILE_ORIENTATION = 0;

    private final byte[][] board = new byte[ROWS][COLS];

    private byte[][] currentTile = new byte[Tetromino.ROWS][Tetromino.COLS];
    private int tileRows;
    private int tileCols;
    private int tileOrientation = 0;
    private int tileType;


    int selectTile() {
        return (int) (Math.random() * 7);
    }


    /**
     * Determines whether it is legal to place the current currentTile ({@link #currentTile}) at the specified
     * row and column.
     *
     * It is legal to place a currentTile if and only if the following are BOTH true:
     *   a. The bounds of the current currentTile will not exceed the play area
     *   b. The current currentTile will not intersect any solid cells already on the board.
     *
     * @param currentRow Uppermost row at which to place the current currentTile.
     * @param currentCol Leftmost column at which to place the current currentTile.
     *
     * @return {@code true} if it is legal to place the current currentTile at the specified row and column.
     */
    private boolean canPlaceTile(final byte[][] tile, int currentRow, int currentCol) {
        final int tileRows = tile.length;
        final int tileCols = tile[0].length;
        final int tileBottom = currentRow + tileRows;
        final int tileRight = currentCol + tileCols;

        if (tileBottom <= ROWS && tileRight <= COLS && currentRow >= 0 && currentCol >= 0) {
            for (int tileRow = 0; tileRow < tileRows; ++tileRow) {
                for (int tileCol = 0; tileCol < tileCols; ++tileCol) {
                    final int boardRow = currentRow + tileRow;
                    final int boardCol = currentCol + tileCol;

                    if (boardRow < ROWS && boardCol < COLS && tile[tileRow][tileCol] != CELL_EMPTY) {
                        if (board[boardRow][boardCol] == CELL_FILLED && tile[tileRow][tileCol] != CELL_EMPTY) {
                            return false;
                        }
                    }
                }
            }

            return true;
        } else {
            return false;
        }

    }

    // TODO Cut out some parts and replace with {@link #canPlaceTile}
    boolean updateCurrentTile(int currentRow, int currentCol) {
        final int bottom = (currentRow + this.tileRows);
        final int right = (currentCol + this.tileCols);

        if (bottom <= ROWS && right <= COLS && currentRow >= 0 && currentCol >= 0) {
            if (this.canPlaceTile(this.currentTile, currentRow, currentCol)) {
                this.clearFalling();
                for (int tileRow = 0; tileRow < tileRows; ++tileRow) {
                    for (int tileCol = 0; tileCol < tileCols; ++tileCol) {
                        final int boardRow = currentRow + tileRow;
                        final int boardCol = currentCol + tileCol;

                        if (boardRow < ROWS && boardCol < COLS) {
                            if (currentTile[tileRow][tileCol] != CELL_EMPTY || board[boardRow][boardCol] != CELL_FILLED) { // Don't overwrite
                                this.board[boardRow][boardCol] = this.currentTile[tileRow][tileCol];
                            }
                        }
                    }
                }

                return true;

            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    boolean isGameOver() {
        return !canPlaceTile(currentTile, 0, (COLS / 2) - 1);

    }

    int checkLines() {
        int score = 0;
        boolean checkAgain = true;

        while (checkAgain) {
            for (int lineRow = 0; lineRow < ROWS; ++lineRow) {
                boolean line = true;
                checkAgain = false;

                for (int lineCol = 0; lineCol < COLS && line; ++lineCol) {
                    line = (board[lineRow][lineCol] == CELL_FILLED);
                }

                // If this row is a line, clear it and shift above down by 1 row
                if (line) {
                    Arrays.fill(board[0], CELL_EMPTY);
                    Arrays.fill(board[lineRow], CELL_EMPTY);

                    for (int row = lineRow - 1; row > 0; --row) {
                        System.arraycopy(board[row], 0, board[row + 1], 0, COLS);
                    }

                    checkAgain = true;

                    // TODO Can increase score based on how many lines we got
                    score += 100;
                }

            }

        }

        return score;
    }


    void makeTile(int type) {
        this.currentTile = Tetromino.getTileData(type, DEFAULT_TILE_ORIENTATION);
        this.tileType = type;
        this.tileRows = this.currentTile.length;
        this.tileCols = this.currentTile[0].length;
        this.tileOrientation = 0;
    }



    /**
     * Empties the game board of all pieces, both falling and landed.
     */
    void clear() {
        for (int boardRow = 0; boardRow < ROWS; ++boardRow) {
            for (int boardCol = 0; boardCol < COLS; ++boardCol) {
                board[boardRow][boardCol] = CELL_EMPTY;
            }
        }
    }

    private void clearFalling() {
        for (int boardRow = 0; boardRow < ROWS; ++boardRow) {
            for (int boardCol = 0; boardCol < COLS; ++boardCol) {
                if (board[boardRow][boardCol] == CELL_FALLING) {
                    board[boardRow][boardCol] = CELL_EMPTY;
                }
            }
        }
    }

    void reInit() {
        for (int row = 0; row < ROWS; ++row) {
            for (int col = 0; col < COLS; ++col) {
                if (board[row][col] == CELL_FALLING) {
                    board[row][col] = CELL_FILLED;
                }
            }
        }
    }

    boolean rotateTile(int currentRow, int currentCol) {
        final int newTileOrientation = (this.tileOrientation + 1) % 4;
        final byte[][] newTile = Tetromino.getTileData(tileType, newTileOrientation);

        if (canPlaceTile(newTile, currentRow, currentCol)) {
            this.tileOrientation = newTileOrientation;
            this.currentTile = newTile;
            this.tileRows = currentTile.length;
            this.tileCols = currentTile[0].length;

            return true;
        }

        return false;
    }

    byte getCell(int row, int col) {
        return this.board[row][col];
    }
}
