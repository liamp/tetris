package ie.ul.tetris.game;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import ie.ul.tetris.Extra;
import ie.ul.tetris.GameOverActivity;
import ie.ul.tetris.MainActivity;


public final class TetrisActivity extends Activity implements GameObserver {
    public static final int DEFAULT_GAME_SPEED = 200;
    private TextView scoreTextView;

    @Override
    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        this.scoreTextView = new TextView(this);
        this.scoreTextView.setTextColor(Color.LTGRAY);
        this.scoreTextView.setTypeface(Typeface.MONOSPACE);
        this.scoreTextView.setTextSize(18.0f);


        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        final int gameSpeed = preferences.getInt(Extra.GAME_SPEED, TetrisActivity.DEFAULT_GAME_SPEED);
        final GLSurfaceView canvas = new TetrisCanvas(this, scoreTextView, gameSpeed);

        this.setContentView(canvas);
        this.addContentView(scoreTextView, new ViewGroup.LayoutParams(256, 256));
    }

    @Override
    public void onBackPressed() {
        final Intent goToMainMenuIntent = new Intent(this.getApplicationContext(), MainActivity.class);
        startActivity(goToMainMenuIntent);
    }

    @Override
    public void onGameOver(int score) {
        final Intent gameOverIntent = new Intent(this.getApplicationContext(), GameOverActivity.class);
        gameOverIntent.putExtra(Extra.SCORE_VALUE, score);
        startActivity(gameOverIntent);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onScoreChange(final int newScore) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                scoreTextView.setText("Score: " + newScore);
            }
        });
    }

    private final class TetrisCanvas extends GLSurfaceView {

        private final Tetris game;

        TetrisCanvas(Context context, final TextView scoreView, final int gameSpeed) {
            super(context);

            this.setEGLContextClientVersion(2);
            this.game = new Tetris(scoreView, gameSpeed);
            this.game.setGameObserver(TetrisActivity.this);
            this.setRenderer(this.game);
        }

        @Override
        public boolean onTouchEvent(MotionEvent e)
        {
            return this.game.processTouchInput(e);
        }

    }
}
