package ie.ul.tetris.game;

final class Tetromino {

    /** No constructor required for static class **/
    private Tetromino() { }


    static final int O = 0;
    static final int I = 1;
    static final int J = 2;
    static final int Z = 3;
    static final int S = 4;
    static final int T = 5;
    static final int L = 6;

    static final int COLS = 4;
    static final int ROWS = 4;


    static final int[] COLOR = {
        0xd3869b, // O
        0xfabd2f, // I
        0x83a598, // J
        0xb8bb26, // Z
        0xfb4934, // S
        0xfe8019, // T
        0xd65d0e  // L
    };


    private static final byte[][][] TILE_O = {
            {{2,2},
             {2,2}},

            {{2,2},
             {2,2}},

            {{2,2},
             {2,2}},

            {{2,2},
             {2,2}},
    };

    private static final byte[][][] TILE_I = {
            {{2},
             {2},
             {2},
             {2}},

            {{2,2,2,2}},

            {{2},
             {2},
             {2},
             {2}},

            {{2,2,2,2}}
    };

    private static final byte[][][] TILE_J = {
            {{2,0,0},
             {2,2,2}},

            {{2,2},
             {2,0},
             {2,0}},

            {{2,2,2},
             {0,0,2}},

            {{0,0,2},
             {2,2,2}}
    };

    private static final byte[][][] TILE_Z = {
            {{2,2,0},
             {0,2,2}},

            {{0,2},
             {2,2},
             {2,0}},

            {{2,2,0},
             {0,2,2}},

            {{0,2},
             {2,2},
             {2,0}},
    };

    private static final byte[][][] TILE_L = {
            {{2,0},
             {2,0},
             {2,2}},

           {{2,2,2},
            {2,0,0}},

           {{2,2},
            {0,2},
            {0,2}},

            {{0,0,2},
             {2,2,2}}
    };

    private static final byte[][][] TILE_T = {
            {{0,2,0},
             {2,2,2}},

            {{2,0},
             {2,2},
             {2,0}},

            {{2,2,2},
             {0,2,0}},

            {{0,2},
             {2,2},
             {0,2}},
    };

    private static final byte[][][] TILE_S = {
            {{0,2,2},
             {2,2,0}},

            {{2,0},
             {2,2},
             {0,2}},

            {{0,2,2},
             {2,2,0}},

            {{2,0},
             {2,2},
             {0,2}},
    };


    static byte[][] getTileData(int type, int orientation) {
        switch (type) {
            case Tetromino.O:
                return TILE_O[orientation];
            case Tetromino.I:
                return TILE_I[orientation];
            case Tetromino.J:
                return TILE_J[orientation];
            case Tetromino.L:
                return TILE_L[orientation];
            case Tetromino.S:
                return TILE_S[orientation];
            case Tetromino.Z:
                return TILE_Z[orientation];
            case Tetromino.T:
                return TILE_T[orientation];
            default:
                return TILE_O[orientation]; // Shouldn't happen
        }
    }
}
