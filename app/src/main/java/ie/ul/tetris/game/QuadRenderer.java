package ie.ul.tetris.game;


import android.opengl.Matrix;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_TRIANGLES;
import static android.opengl.GLES20.glDisableVertexAttribArray;
import static android.opengl.GLES20.glDrawArrays;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glUniform3fv;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.GLES20.glUseProgram;
import static android.opengl.GLES20.glVertexAttribPointer;

// TODO Maybe use a single large ByteBuffer to fix #7
final class QuadRenderer {

    private static final int BYTES_PER_FLOAT = 4;
    private static final int MAX_QUADS = 512;

    private final int renderShader;

    private final QuadData[] quads = new QuadData[MAX_QUADS];

    private final int transformUniform;
    private final int positionAttr;
    private final int colorUniform;


    private final float[] transform = new float[16];
    private int quadCount;

    QuadRenderer(int renderShader, int initialWidth, int initialHeight) {
        this.quadCount = 0;
        this.renderShader = renderShader;

        glUseProgram(this.renderShader);
        this.transformUniform = glGetUniformLocation(this.renderShader, "transform");
        this.positionAttr = glGetAttribLocation(this.renderShader, "vPosition");
        this.colorUniform = glGetUniformLocation(this.renderShader,"vColor");
        glUseProgram(0);
    }

    void updateTransform(int width, int height ) {
        Matrix.orthoM(this.transform, 0, 0, width, 0, height, 0.0f, 100.0f);
    }

    void clear() {
        this.quadCount = 0;
    }

    void draw() {
        glUseProgram(this.renderShader);
        glUniformMatrix4fv(this.transformUniform, 1, false, this.transform, 0);

        for (int quadIndex = 0; quadIndex < this.quadCount; ++quadIndex) {
            final QuadData quad = this.quads[quadIndex];
            glEnableVertexAttribArray(this.positionAttr);
            glVertexAttribPointer(this.positionAttr, 2, GL_FLOAT, false, 2 * BYTES_PER_FLOAT, quad.vertexData);

            glUniform3fv(this.colorUniform, 1, quad.colorData);

            glDrawArrays(GL_TRIANGLES, 0, 6);
            glDisableVertexAttribArray(this.positionAttr);
        }

        glUseProgram(0);
    }

    void pushQuad(float x0, float y0, float x1, float y1, final int color) {

        if (this.quadCount < MAX_QUADS) {
            float crRed = ((color >> 16) & 0xff) * 1.0f / 255.0f;
            float crGreen = ((color >> 8) & 0xff) * 1.0f / 255.0f;
            float crBlue = ((color) & 0xff) * 1.0f / 255.0f;

            // TODO Can use an index buffer here
            final float vertices[] = {
                    x0, y0,
                    x1, y0, // First triangle
                    x0, y1,

                    x0, y1,
                    x1, y0, // Second triangle
                    x1, y1
            };

            final float colorComponents[] = {crRed, crGreen, crBlue};

            ByteBuffer rawVerticesBuffer = ByteBuffer.allocateDirect(vertices.length * BYTES_PER_FLOAT);
            rawVerticesBuffer.order(ByteOrder.nativeOrder());
            FloatBuffer vertexBuffer = rawVerticesBuffer.asFloatBuffer();
            vertexBuffer.put(vertices);
            vertexBuffer.position(0);

            ByteBuffer rawColorBuffer = ByteBuffer.allocateDirect(colorComponents.length * BYTES_PER_FLOAT);
            rawColorBuffer.order(ByteOrder.nativeOrder());
            FloatBuffer colorBuffer = rawColorBuffer.asFloatBuffer();
            colorBuffer.put(colorComponents);
            colorBuffer.position(0);

            // FIXME: Memory leak here, see issue #7
            QuadData quad = new QuadData(vertexBuffer, colorBuffer);

            this.quads[this.quadCount] = quad;
            ++this.quadCount;
        } else {
            // Not really necessary to throw an exception here because Tetris should never generate
            // more than 256 quads.
            Log.e("Tetris/Render", "Exceeded quad push size!");
        }
    }


    private final class QuadData {
        final FloatBuffer vertexData;
        final FloatBuffer colorData;

        QuadData(FloatBuffer vertices, FloatBuffer colors) {
            this.vertexData = vertices;
            this.colorData = colors;
        }
    }
}
