package ie.ul.tetris.game;

interface GameObserver {
    void onGameOver(final int score);

    void onScoreChange(final int newScore);
}
