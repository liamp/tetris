package ie.ul.tetris.game;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;
import android.widget.TextView;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import static android.opengl.GLES10.glClear;
import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.glClearColor;
import static android.opengl.GLES20.glUseProgram;
import static android.opengl.GLES20.glViewport;

final class Tetris implements GLSurfaceView.Renderer {

    private static final int MS_BETWEEN_DOUBLETAP = 200;
    private static final float CELL_WIDTH_RATIO = 0.0555f;

    private final GameBoard board;
    private final TextView scoreTextView;
    private QuadRenderer renderer;
    private int screenWidth;
    private int screenHeight;
    private int currentRow;
    private int currentCol;
    private float cellWidthPx;
    private long lastUpdateTime;
    private int inputColumnsToMove = 0;
    private boolean hasTapInput = false;
    private boolean hasDragInput = false;
    private long lastTapTime;
    private int lastTouchCol;
    private GameObserver gameObserver;
    private float boardXOffset = 0.0f;
    private float boardYOffset = 0.0f;
    private int score;
    private boolean gameOver;
    private int millisecondsPerTick = TetrisActivity.DEFAULT_GAME_SPEED;


    @SuppressLint("SetTextI18n")
    Tetris(final TextView scoreView, final int millisecondsPerTick) {
        this.board = new GameBoard();
        this.millisecondsPerTick = millisecondsPerTick;
        this.scoreTextView = scoreView;
        this.scoreTextView.setText("Score: " + this.score);
    }

    private void startGame() {
        this.board.clear();
        this.board.makeTile(this.board.selectTile());
        this.currentRow = 0;
        this.currentCol = (GameBoard.VISIBLE_COLS / 2);
        this.lastTouchCol = 0;
    }


    // Called from RENDER thread.
    private void updateBoard() {
        if (board.isGameOver()) {
            this.gameOver = true;
            this.gameObserver.onGameOver(this.score);
        } else {
            if (this.board.updateCurrentTile(currentRow, currentCol)) {
                ++this.currentRow;
            } else {
                board.reInit();
                this.score += board.checkLines();
                board.makeTile(board.selectTile());
                gameObserver.onScoreChange(score);
                currentRow = 0;
                currentCol = (GameBoard.VISIBLE_COLS / 2) - 1;
            }
        }
    }


    private void drawBoard() {
        final float boardTop = (screenHeight - boardYOffset);
        final float boardBottom = (boardTop - (GameBoard.VISIBLE_ROWS * cellWidthPx));
        final float boardRight = (boardXOffset + (GameBoard.VISIBLE_COLS * cellWidthPx));

        for (int row = 0; row < GameBoard.VISIBLE_ROWS; ++row) {
            for (int col = 0; col < GameBoard.VISIBLE_COLS; ++col) {
                final byte cellValue = board.getCell(4 + row, col);
                if (cellValue != 0) {
                    float x0 = boardXOffset + (col * cellWidthPx);
                    float y0 = boardTop - (row * cellWidthPx);
                    float x1 = x0 + cellWidthPx;
                    float y1 = y0 - cellWidthPx;
                    int color = Tetromino.COLOR[cellValue];
                    this.renderer.pushQuad(x0, y0, x1, y1, color);
                }
            }
        }

        // Draw border lines
        this.renderer.pushQuad((boardXOffset - 6.0f), boardBottom, boardXOffset, boardTop, 0x5c5c5c);
        this.renderer.pushQuad(boardRight, boardTop, (boardRight + 6.0f), boardBottom, 0x5c5c5c);
        this.renderer.pushQuad(boardXOffset, boardTop, boardRight, (boardTop + 6.0f), 0x5c5c5c);
        this.renderer.pushQuad(boardXOffset, (boardBottom - 6.0f), boardRight, boardBottom, 0x5c5c5c);
    }

    // NOTE: Called from MAIN thread.
    boolean processTouchInput(final MotionEvent e) {
        final float touchX = e.getRawX();
        final int touchCol = (int)((touchX - boardXOffset) / cellWidthPx);

        switch (e.getAction())
        {
            case MotionEvent.ACTION_MOVE:
            {
                this.inputColumnsToMove = touchCol - lastTouchCol;
                this.hasDragInput = true;
            } break;

            case MotionEvent.ACTION_DOWN:
            {
                long tapTime = System.currentTimeMillis();
                if ((tapTime - lastTapTime) <= MS_BETWEEN_DOUBLETAP) {
                    this.hasTapInput = true;
                }

                this.lastTapTime = tapTime;
            } break;
        }

        this.lastTouchCol = touchCol;
        return true;
    }


    @Override
    public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
        glClearColor(0.24f, 0.24f, 0.24f, 1.0f);

        int shader = Shaders.compile(Shaders.MAIN_VERTEX, Shaders.MAIN_FRAGMENT);
        if (shader != 0) {
            glUseProgram(shader);
        }

        this.screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
        this.screenHeight = Resources.getSystem().getDisplayMetrics().heightPixels;

        this.boardXOffset = (screenWidth - (GameBoard.VISIBLE_COLS * cellWidthPx)) / 2;
        this.boardYOffset = (screenHeight - (GameBoard.VISIBLE_ROWS * cellWidthPx)) / 2;


        this.renderer = new QuadRenderer(shader, screenWidth, screenHeight);

        this.startGame();
    }

    @Override
    public void onSurfaceChanged(GL10 gl10, int width, int height) {
        glViewport(0, 0, width, height);
        this.renderer.updateTransform(width, height);

        this.screenWidth = width;
        this.screenHeight = height;

        this.cellWidthPx = this.screenWidth * CELL_WIDTH_RATIO;
        this.boardXOffset = (this.screenWidth - (GameBoard.VISIBLE_COLS * cellWidthPx)) / 8;
        this.boardYOffset = (this.screenHeight - (GameBoard.VISIBLE_ROWS * cellWidthPx)) / 2;

        final float scoreTextX = ((this.screenWidth - this.screenWidth/3));
        final float scoreTextY = ((256.0f) - 32.0f);
        this.scoreTextView.setX(scoreTextX);
        this.scoreTextView.setY(scoreTextY);
    }

    @Override
    // NOTE: Called from RENDER thread
    public void onDrawFrame(GL10 gl10) {
        glClear(GL_COLOR_BUFFER_BIT);
        renderer.clear();
        drawBoard();
        renderer.draw();

        long timeNow = System.currentTimeMillis();

        if (!this.gameOver) {
            if (timeNow - lastUpdateTime >= millisecondsPerTick) {
                this.updateBoard();
                this.lastUpdateTime = timeNow;
            } else {
                if (this.hasDragInput) {
                    if (board.updateCurrentTile(currentRow, currentCol + inputColumnsToMove)) {
                        this.currentCol += this.inputColumnsToMove;
                        this.hasDragInput = false;
                        this.inputColumnsToMove = 0;
                    }
                }

                if (this.hasTapInput) {
                    if (board.rotateTile(currentRow, currentCol)) {
                        this.board.updateCurrentTile(currentRow, currentCol);
                    }

                    this.hasTapInput = false;
                }

                try {
                    Thread.sleep(1); // Don't eat CPU
                } catch (InterruptedException e) {
                    // Ignore
                }
            }
        }


    }

    void setGameObserver(GameObserver gameObserver) {
        this.gameObserver = gameObserver;
    }
}
