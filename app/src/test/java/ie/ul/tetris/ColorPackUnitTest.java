package ie.ul.tetris;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Used to test the logic for unpacking RGB integer colors into float vectors.
 */
public class ColorPackUnitTest {
    @Test
    public void unpackColorCorrect() {
        final int color = 0xff0000; // Pure red

        float crRed = ((color >> 16) & 0xff) * 1.0f / 255.0f;
        float crGreen = ((color >> 8) & 0xff) * 1.0f / 255.0f;
        float crBlue = ((color) & 0xff) * 1.0f / 255.0f;

        assertEquals(1.0f, crRed, 0.0f);
        assertEquals(0.0f, crBlue, 0.0f);
        assertEquals(0.0f, crGreen, 0.0f);
    }
}
